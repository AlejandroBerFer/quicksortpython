class PruebaQuicksort:
    def QuickSort(self, numeros, izq, der):
        pivote = numeros[izq]
        i = izq
        j = der
        aux = 0
        while i < j:
            while numeros[i] <= pivote and i < j:
                i += 1
            while numeros[j] > pivote:
               j -= 1
            if i < j:
              aux = numeros[i]
              numeros[i] = numeros[j]
              numeros[j] = aux
        numeros[izq] = numeros[j]
        numeros[j] = pivote
        if izq < j-1:
            self.QuickSort(numeros,izq,j-1)
        if j+1 < der:
            self.QuickSort(numeros,j+1,der)


quick = PruebaQuicksort();
numeros = [10,12,5,2,8,3,1,7,22,6,30,4,11,2,9]
print("Sin ordenar: "+str(numeros))
quick.QuickSort(numeros,0,len(numeros) - 1)
print("\nOrdenados: "+str(numeros))